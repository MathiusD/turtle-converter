#!/bin/bash

buildDir=$1
mkdir -p "$buildDir"
for lang in $(ls locale); do
    sphinx-build . "$buildDir"/"$lang" -D language="$lang"
done
