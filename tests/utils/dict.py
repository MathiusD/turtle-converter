from unittest import TestCase

from parameterized import parameterized
from turtle_converter.utils import dict as dct


class TestUtilsDict(TestCase):

    @parameterized.expand([
        ([], None, None),
        ([], 'ex', 'ex'),
        (['Example'], None, {'Example': None}),
        (['Example'], 'ex', {'Example': 'ex'}),
        (['Example', 'Truc'], None, {'Example': None, 'Truc': None}),
        (['Example', 'Truc'], 'ex', {'Example': 'ex', 'Truc': 'ex'}),
        ([['Example', 'Ex'], 'Truc'], None, {
         'Example': {'Ex': None}, 'Truc': None}),
        ([['Example', 'Ex'], 'Truc'], 'ex', {
         'Example': {'Ex': 'ex'}, 'Truc': 'ex'})
    ])
    def testDictify(self, lst: list, default: object, expected: dict):
        self.assertEqual(dct.dictify(lst, default), expected)

    @parameterized.expand([
        ({}, None, None, {}),
        ({}, 'ex', 'ex', {'ex': 'ex'}),
        ({}, ['ex', 'ex'], None, {'ex': {'ex': None}}),
        ({'ex': {}}, ['ex', 'ex'], None, {'ex': {'ex': None}}),
        ({'ex': {}}, [], None, {'ex': {}}),
        ({'ex': {}}, ['ex'], {'nop': 'ex'}, {'ex': {'nop': 'ex'}}),
        ({'ex': {}}, ['ex'], 'Example', {'ex': 'Example'})
    ])
    def testAddToDict(self, currentDict: dict, key: list, value: object, expected: dict):
        self.assertEqual(dct.addToDict(currentDict, key, value), expected)
