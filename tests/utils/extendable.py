from unittest import TestCase

from parameterized import parameterized
from turtle_converter.utils import extendable


class TestUtilsExtendable(TestCase):

    class Mock(extendable.Extendable):

        def __init__(self, data: dict = {}):
            for key in data.keys():
                self.__setattr__(key, data.get(key))

    @parameterized.expand([
        (Mock(), 'example', None, None, None, None),
        (Mock(), 'example', None, {}, None, None),
        (Mock(), 'example', None, Mock(), None, None),
        (Mock(), 'example', 'ex', Mock(), None, 'ex'),
        (Mock(), 'ex', None, Mock({'ex': 'ex'}), None, 'ex'),
        (Mock(), 'ex', 'example', Mock({'ex': 'ex'}), None, 'ex'),
        (Mock({'ex': 'def'}), 'ex', 'example', Mock({'ex': 'ex'}), None, 'ex'),
        (Mock({'ex': None}), 'ex', 'example', Mock({'ex': 'ex'}), None, 'ex'),
        (Mock({'ex': None}), 'ex', 'example', {'ex': 'ex'}, None, 'ex'),
        (Mock({'ex': None}), 'ex', 'example', None, None, 'example'),
        (Mock({'nop': Mock({'ex': 'ex'})}),
         'ex', 'example', None, 'nop', 'ex'),
        (Mock({'nop': [Mock({'ex': 'ex'})]}),
         'ex', 'example', None, 'nop', 'ex'),
        (Mock({'nop': [Mock(), Mock({'ex': 'ex'})]}),
         'ex', 'example', None, 'nop', 'ex'),
        (Mock({'nop': [Mock({'ex': 'notEx'}), Mock({'ex': 'ex'})]}),
         'ex', 'example', None, 'nop', 'notEx')
    ])
    def testGet(self, obj: extendable.Extendable, key: str, default: object, extendable: extendable.Extendable or dict, insideExtendableName: str, expectedValue: object):
        self.assertEqual(obj.get(key, default, extendable,
                         insideExtendableName), expectedValue)
