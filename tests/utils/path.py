import os
from unittest import TestCase
from unittest.mock import MagicMock

from parameterized import parameterized
from turtle_converter.utils import path


class TestUtilsPath(TestCase):

    @parameterized.expand([
        ('example.foo', None, {}, './example.foo'),
        ('example.foo', 'foo', {'foo': './dir'}, './dir/example.foo'),
        ('example.foo', 'foo', {'notFoo': './dir'}, './example.foo'),
        ('example.foo', 'dir', {'dir': './dir'}, './dir/example.foo'),
        ('example.foo', None, {'dir': './dir',
         'ensure': True}, './dir/example.foo', './dir/'),
        ('example.foo', None, {'dir': './dir',
         'ensure': False}, './dir/example.foo')
    ])
    def testGetPath(self, pathUsed: str, key: str, kwargsRelated: dict, result: str, pathCreated: str = None):
        os.path.curdir = '.'
        os.path.sep = '/'
        os.makedirs = MagicMock()
        if key:
            self.assertEqual(path.getPath(
                pathUsed, key, **kwargsRelated), result)
        else:
            self.assertEqual(path.getPath(pathUsed, **kwargsRelated), result)
        if pathCreated:
            os.makedirs.assert_called_once_with(pathCreated, exist_ok=True)
        else:
            os.makedirs.assert_not_called()
