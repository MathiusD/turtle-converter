from unittest import TestCase

from parameterized import parameterized
from turtle_converter.utils import check


class TestUtilsCheck(TestCase):

    class TestInputData:

        def __init__(self, data: dict, key: str, msg: str = None, kwargRelated: dict = {}):
            self.data = data
            self.key = key
            self.msg = msg
            self.kwargRelated = kwargRelated if kwargRelated else {}

    class TestExceptionData:

        def __init__(self, expectedException=None, expectedMsg: str = None):
            self.expectedException = expectedException
            self.expectedMsg = expectedMsg

    @parameterized.expand([
        (TestInputData({}, 'example', kwargRelated={'class': TestInputData}), TestExceptionData(
            AttributeError, "'example' must be defined in TestInputData data")),
        (TestInputData({}, 'example'), TestExceptionData(
            AttributeError, "'example' must be defined in  data")),
        (TestInputData({}, 'example', 'Error occured !'), TestExceptionData(
            AttributeError, "Error occured !")),
        (TestInputData({'example': 'example'}, 'example'), None)
    ])
    def testDataIsRequired(self, data: TestInputData, exception: TestExceptionData):
        if exception:
            with self.assertRaises(exception.expectedException) as error:
                if data.msg:
                    check.dataIsRequired(data.key, data.data,
                                         data.msg, **data.kwargRelated)
                else:
                    check.dataIsRequired(
                        data.key, data.data, **data.kwargRelated)
            self.assertEqual(error.exception.args[0],
                             exception.expectedMsg)
            self.assertNotIn(data.key, data.data.keys())
        else:
            check.dataIsRequired(data.key, data.data)
            self.assertIn(data.key, data.data.keys())

    @parameterized.expand([
        (TestInputData({}, 'example', kwargRelated={'class': TestInputData}), None, TestExceptionData(
            AttributeError, "'example' must be defined in TestInputData data")),
        (TestInputData({}, 'example'), None, TestExceptionData(
            AttributeError, "'example' must be defined in  data")),
        (TestInputData({}, 'example', 'Error occured !'), None, TestExceptionData(
            AttributeError, "Error occured !")),
        (TestInputData({'example': 'example'}, 'example'), 'example', None)
    ])
    def testGetRequiredData(self, data: TestInputData, value: object, exception: TestExceptionData):
        if exception:
            with self.assertRaises(exception.expectedException) as error:
                if data.msg:
                    check.dataIsRequired(data.key, data.data,
                                         data.msg, **data.kwargRelated)
                else:
                    check.dataIsRequired(
                        data.key, data.data, **data.kwargRelated)
            self.assertEqual(error.exception.args[0],
                             exception.expectedMsg)
            self.assertNotIn(data.key, data.data.keys())
        else:
            self.assertEqual(check.getRequireData(data.key, data.data), value)
            self.assertIn(data.key, data.data.keys())
