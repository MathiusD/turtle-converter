import abc
import re

from ..utils import ClassWithKeyAssociated


class BaseFetcher(ClassWithKeyAssociated):

    @classmethod
    def keyAssociated(cls):
        return cls.protocolsSupported()

    @staticmethod
    @abc.abstractmethod
    def protocolsSupported():
        pass

    @staticmethod
    def protocolOfUri(uri: str):
        search = re.search("[a-z]*:\/\/", uri)
        return search.group(0).replace("://", "") if search else None

    @classmethod
    def support(cls, protocol: str):
        return protocol in cls.protocolsSupported()

    @classmethod
    @abc.abstractmethod
    def write(cls, uri: str, target: str, **kwargs):
        pass
