import logging
import os
import shutil

from ..utils import log
from .base import BaseFetcher


class FileFetcher(BaseFetcher):

    @staticmethod
    def protocolsSupported():
        return [
            "file"
        ]

    @classmethod
    def write(cls, uri: str, target: str, **kwargs):
        origin = uri.replace("file://", "")
        log(logging.INFO, "Fetch file from file system at %s" % origin)
        if not os.path.exists(origin):
            log(logging.INFO, "File not found at %s" % origin)
            return False
        else:
            shutil.copy(origin, target)
            return True
