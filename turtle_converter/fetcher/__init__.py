import copy

from ..utils import getPath, makeKeyDict
from .base import BaseFetcher
from .file import FileFetcher
from .requests import HttpFetcher

_fetchers = [
    HttpFetcher,
    FileFetcher
]

fetchers = makeKeyDict(_fetchers)


def protocolOfUri(uri: str):
    return BaseFetcher.protocolOfUri(uri)


def fetch(uri: str, path: str, **kwargs):
    pathKwargs = copy.deepcopy(kwargs)
    pathKwargs["ensure"] = True
    fullPath = getPath(path, "inDir", **pathKwargs)
    protocol = protocolOfUri(uri)
    if protocol in fetchers.keys():
        return fetchers[protocol][0].write(uri, fullPath)
    else:
        raise Exception(
            "Not fetcher register for %s protocol" % protocol)


__all__ = [
    "fetchers",
    "protocolOfUri",
    "fetch"
]
