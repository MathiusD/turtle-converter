import argparse
import logging
import os
import tempfile
from enum import Enum

from turtle_converter import ConfLoader
from turtle_converter.utils import getLogLevel, log


class CmdKeyWord(Enum):
    LOAD = "load"
    BUILD = "build"
    UPLOAD = "upload"
    VALIDATE = "validate"


parser = argparse.ArgumentParser(
    description="For generate Ttl files from other source")
parser.add_argument("-c", "--conf", dest="conf", default="conf.yml", type=str,
                    help="Configuration file used (Default conf.yml)")
parser.add_argument("-y", "--autoConfirm", dest="autoConfirm", action="store_true", default=None,
                    help="For auto accept any question in converter. Warning may be erase old generation and auto push ttl (Default not set and load from conf)")
parser.add_argument("-v", "--verbosity", dest="verbosity", type=str,
                    help="Verbosity of application (Default INFO and overriding by conf if isn't specified)")
parser.add_argument("-o", "--only", dest="only", default=None, type=str, nargs='+',
                    help="For process with only specific target (Default all)")
parser.add_argument("cmd", metavar="instruction", type=CmdKeyWord, nargs='+',
                    help="instruction(s) used in this script (Example `build upload`)")

args = parser.parse_args()

verbositySpecified = args.verbosity is not None
verbosity = getLogLevel(args.verbosity)

logging.basicConfig(level=verbosity, format='%(asctime)s:%(levelname)s:%(message)s',
                    filename="%s%s%s.log" % (
                        tempfile.gettempdir(), os.path.sep,
                        os.path.basename(
                            os.path.abspath(os.path.curdir))
                    ))

try:
    conf = ConfLoader.loadConf(args.conf, args.autoConfirm,
                               CmdKeyWord.UPLOAD in args.cmd, verbositySpecified)
except AttributeError as e:
    print("Conf is invalid for cause : %s" % e.args)
    exit(1)

for keyWord in args.cmd:
    if keyWord == CmdKeyWord.BUILD:
        conf.writeTtl(args.only)
    elif keyWord == CmdKeyWord.UPLOAD:
        if conf.serverRelated:
            conf.sendTtl(args.only)
        else:
            log(logging.INFO, "KeyWord ignored because no server is renseigned inside %s" % args.conf)
    elif keyWord == CmdKeyWord.VALIDATE:
        print("Conf is valid")
    elif keyWord == CmdKeyWord.LOAD:
        loaded = conf.loadTargets(args.only)
        for target in loaded:
            loadedNb = len(loaded[target])
            log(logging.INFO, "%s loaded with %s entities.%s" %
                (target.name, loadedNb, " Sample data is : %s" % loaded[target][0] if loadedNb else ""))
