from csv import DictReader

from .base import BaseLoader


class CSVLoader(BaseLoader):

    @staticmethod
    def typesSupported():
        return [
            "csv"
        ]

    @classmethod
    def load(cls, path: str, **kwargs):
        if not cls.exist(path):
            raise AttributeError("Target %s not found !" % path)
        data = []
        with open(path) as file:
            reader = DictReader(file, delimiter=kwargs.get('delimiteur', ';'))
            for row in reader:
                data.append(row)
            return data
