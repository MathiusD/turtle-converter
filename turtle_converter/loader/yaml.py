from .base import BaseLoader
import yaml


class YamlLoader(BaseLoader):

    @staticmethod
    def typesSupported():
        return [
            "yml",
            "yaml"
        ]

    @classmethod
    def load(cls, path: str):
        if not cls.exist(path):
            raise AttributeError("Target %s not found !" % path)
        with open(path, 'r') as file:
            return yaml.load(file, Loader=yaml.FullLoader)
