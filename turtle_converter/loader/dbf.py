from .base import BaseLoader
from dbfread import DBF


class DBFLoader(BaseLoader):

    @staticmethod
    def typesSupported():
        return [
            "dbf"
        ]

    @classmethod
    def load(cls, path: str, **kwargs):
        if not cls.exist(path):
            raise AttributeError("Target %s not found !" % path)
        return DBF(path, load=True, encoding=kwargs.get("encoding", "utf-8")).records
