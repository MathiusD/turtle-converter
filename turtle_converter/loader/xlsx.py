import copy
from enum import Enum

from openpyxl import load_workbook

from ..utils import addToDict, dictify, getRequireData
from .base import BaseLoader


class XlsxLoader(BaseLoader):

    @staticmethod
    def typesSupported():
        return [
            "xlsx"
        ]

    @classmethod
    def load(cls, path: str, **kwargs):
        if not cls.exist(path):
            raise AttributeError("Target %s not found !" % path)
        workbook = load_workbook(path, True)
        data = []
        keys = {}
        keyOn = kwargs.get("keyOn", [1])
        readByRow = kwargs.get("readByRows", True)
        sheet = getRequireData(
            "sheet", kwargs, "'$key' must be defined for load this xlsx file.")
        if sheet in workbook.sheetnames:
            worksheet = workbook[sheet]
            for indexKey in keyOn:
                for index, entity in enumerate(worksheet.iter_rows(min_row=indexKey, max_row=indexKey, values_only=True) if readByRow else worksheet.iter_cols(min_col=indexKey, max_col=indexKey, values_only=True)):
                    lastCell = None
                    for internalIndex, cell in enumerate(entity):
                        if not cell and lastCell:
                            cell = lastCell
                        if cell:
                            if internalIndex not in keys.keys():
                                keys[internalIndex] = []
                            keys[internalIndex].append(cell)
                            lastCell = cell
            listKey = []
            for key in keys.values():
                listKey.append(key)
            for index, entity in enumerate(worksheet.iter_rows(values_only=True) if readByRow else worksheet.iter_cols(values_only=True)):
                if not index + 1 in keyOn:
                    dat = dictify(listKey)
                    for internalIndex, cell in enumerate(entity):
                        if cell:
                            addToDict(dat, keys[internalIndex], cell)
                    data.append(dat)
            return data
        else:
            raise AttributeError("'%s' not found in %s file" % (sheet, path))
