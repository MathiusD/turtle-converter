from .base import BaseLoader
import json


class JsonLoader(BaseLoader):

    @staticmethod
    def typesSupported():
        return [
            "json",
            "json-ld"
        ]

    @classmethod
    def load(cls, path: str):
        if not cls.exist(path):
            raise AttributeError("Target %s not found !" % path)
        with open(path, "r") as file:
            return json.load(file)
