import abc
import os

from ..utils import ClassWithKeyAssociated


class BaseLoader(ClassWithKeyAssociated):

    @classmethod
    def keyAssociated(cls):
        return cls.typesSupported()

    @staticmethod
    @abc.abstractmethod
    def typesSupported():
        pass

    @classmethod
    def support(cls, typeProvided: str):
        return typeProvided in cls.typesSupported()

    @staticmethod
    def exist(path: str):
        return os.path.exists(path)

    @classmethod
    @abc.abstractmethod
    def load(cls, path: str, **kwargs):
        pass
