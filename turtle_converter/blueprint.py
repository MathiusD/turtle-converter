import copy

from .targetable import Targetable


class Blueprint(Targetable):

    def __init__(self, blueprintData: dict, prefix: list = None, blueprints: dict = {}):
        customKwargs = copy.deepcopy(blueprintData) if blueprintData else {}
        if prefix:
            customKwargs["commonPrefix"] = prefix
        if "blueprint" in customKwargs.keys():
            if type(customKwargs["blueprint"]) is list:
                blueprint = []
                for name in customKwargs["blueprint"]:
                    blueprint.append(blueprints.get(name))
            else:
                blueprint = blueprints.get(
                    customKwargs["blueprint"])
            if blueprint:
                customKwargs["blueprint"] = blueprint
        super().__init__(**customKwargs)
