import copy
import json
import logging
import os
import shutil

import requests

from .loader import load
from .targetable import Targetable
from .turtle import TripleData, Triple, generatePrefix
from .utils import (BaseClass, askBoolean, dataIsRequired, getPath, log,
                    showPercentage, showSize)


class Target(Targetable, BaseClass):

    def __init__(self, targetData: dict, prefix: list = None, blueprints: dict = {}):
        dataIsRequired(
            "name", targetData, **self.constructorKwargs)
        customKwargs = copy.deepcopy(targetData)
        if prefix:
            customKwargs["commonPrefix"] = prefix
        if "blueprint" in customKwargs.keys():
            if type(customKwargs["blueprint"]) is list:
                blueprint = []
                for name in customKwargs["blueprint"]:
                    blueprint.append(blueprints.get(name))
            else:
                blueprint = blueprints.get(
                    customKwargs["blueprint"])
            if blueprint:
                customKwargs["blueprint"] = blueprint
        super().__init__(**customKwargs)
        dataIsRequired("url", self.__dict__, **self.constructorKwargs)

    def loadTarget(self, **kwargs):
        fullPath = getPath(self.rawPath, "inDir", **kwargs)
        log(logging.DEBUG, "Load %s" % fullPath)
        targetExist = os.path.exists(fullPath)
        if not targetExist:
            log(logging.DEBUG, "Not found locally start fetch from %s" % self.url)
            targetExist = self.fetch(**kwargs)
        if not targetExist:
            raise Exception("Target %s not found" % fullPath)
        else:
            log(logging.DEBUG, "Target %s found !" % fullPath)
            return load(fullPath, self.rawFormat, **self.loaderOpts)

    def generateTtl(self, **kwargs):
        ttl = []
        log(logging.INFO, "Generate Ttl Prefix")
        for prefix in self.allPrefix:
            ttl.append(generatePrefix(prefix.name, prefix.uri))
        if self.constantTriples != None:
            log(logging.INFO, "Add Constants Triples")
            for constantTriple in self.constantTriples:
                ttl.append(constantTriple.generateTriple().value)
        dataLoaded = self.loadTarget(**kwargs)
        dataSize = len(dataLoaded)
        nbTriples = 0
        for index, entity in enumerate(dataLoaded):
            if logging.root.level <= logging.INFO:
                showPercentage(index + 1, dataSize, "Generate Ttl Data ")
            entityIsValid = True
            for retainedField in self.retainedFields:
                entityIsValid &= retainedField.isRetained(entity)
                if not entityIsValid:
                    break
            if entityIsValid:
                tripleDataList = []
                for field in self.fields:
                    value = field.getFromEntity(entity, **kwargs)
                    if value:
                        tripleDataList.append(TripleData(
                            field.relation, value, field.type, field.isLink, field.isRDF))
                for constant in self.constantFields:
                    tripleDataList.append(TripleData(
                        constant.relation, constant.value, constant.type, constant.isLink, constant.isRDF))
                genTriple = Triple(
                    "%s%s" % (self.nameDataPrefix, index), tripleDataList).generateTriple()
                nbTriples += genTriple.nbTriples
                ttl.append(genTriple.value)
        log(logging.INFO, "%s Ttl data generated" % nbTriples)
        return ttl

    def writeTtl(self, **kwargs):
        pathKwargs = copy.deepcopy(kwargs)
        pathKwargs["ensure"] = True
        path = getPath(self.outPath, "outDir", **pathKwargs)
        if os.path.exists(path):
            if askBoolean("File %s exist. Removing ?\n" % path, **kwargs):
                print("File removing")
                os.remove(path)
            else:
                print("Aborting.")
                return False
        with open(path, 'w') as out:
            ttlData = self.generateTtl(**kwargs)
            log(logging.DEBUG, "Writing ttl started at %s" % path)
            for data in ttlData:
                out.write("%s\n" % data)
            return True
