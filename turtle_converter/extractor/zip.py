from .base import BaseExtractor

from zipfile import ZipFile


class ZipExtractor(BaseExtractor):

    @staticmethod
    def typesSupported():
        return [
            "zip"
        ]

    @classmethod
    def extract(cls, path: str, target: str, **kwargs):
        if not cls.exist(path):
            raise AttributeError("Target %s not found !" % path)
        with ZipFile(path) as archive:
            archive.extractall(target)
            return True
