import logging
import os

from .extractor import extract
from .fetchable import Fetchable
from .utils import BaseClass, getPath, getRequireData, log


class Requirement(Fetchable, BaseClass):

    def __init__(self, requirementData: dict):
        self.url = getRequireData(
            "url", requirementData, **self.constructorKwargs)
        self.name = getRequireData(
            "name", requirementData, **self.constructorKwargs)
        self.type = getRequireData(
            "type", requirementData, **self.constructorKwargs)
        self.requirements = []
        for requirement in requirementData.get("requirements", []):
            self.requirements.append(Requirement(requirement))
        super().__init__(self.url, self.archivePath, self.type, self.requirements)

    @property
    def archivePath(self):
        return "%s.%s" % (self.name, self.type)

    def usable(self, **kwargs):
        if not os.path.exists(getPath(self.name, "inDir", **kwargs)):
            return self.extract(**kwargs)
        return True

    def extract(self, **kwargs):
        fullPath = getPath(self.archivePath, "inDir", **kwargs)
        log(logging.DEBUG, "Extract %s" % fullPath)
        targetExist = os.path.exists(fullPath)
        if not targetExist:
            log(logging.DEBUG, "Not found locally start fetch from %s" % self.url)
            targetExist = self.fetch(**kwargs)
        if not targetExist:
            raise Exception("Target %s not found" % fullPath)
        else:
            return extract(fullPath, getPath(self.name, "inDir", **kwargs))
