import abc


class ClassWithKeyAssociated:

    @classmethod
    @abc.abstractmethod
    def keyAssociated(cls):
        pass


def makeKeyDict(selectors: list):
    out = {}
    for selector in selectors:
        if issubclass(selector, ClassWithKeyAssociated):
            for key in selector.keyAssociated():
                if not key in out.keys():
                    out[key] = []
                out[key].append(selector)
    return out
