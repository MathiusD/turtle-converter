import logging


def log(lvl: int, msg: str):
    if logging.root.isEnabledFor(lvl):
        print(msg)
        logging.log(lvl, msg)


def getLogLevel(lvl: str, default: int = logging.INFO):
    if lvl and type(lvl) == str and lvl.upper() in logging._nameToLevel:
        return logging._nameToLevel[lvl.upper()]
    else:
        return default
