class Extendable:

    def get(self, propertyName: str, default: object = None, extendable: dict = None, insideExtendableName: str = None):
        value = extendable.get(propertyName) if extendable else None
        if not value:
            value = self.__getattribute__(
                propertyName) if propertyName in self.__dict__.keys() else None
            if not value and insideExtendableName in self.__dict__.keys():
                insideExtendable = self.__getattribute__(insideExtendableName)
                if type(insideExtendable) is list:
                    for ext in insideExtendable:
                        value = ext.get(propertyName)
                        if value:
                            break
                else:
                    value = insideExtendable.get(propertyName)
        return value if value else default
