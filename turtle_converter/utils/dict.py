def addToDict(dct: dict, key: list or str, value: object):
    if key:
        keyIsList = type(key) is list
        subDict = dictify(key[1:], value) if keyIsList else value
        index = key[0] if keyIsList else key
        if index in dct:
            if type(subDict) is dict:
                for internalKey in subDict:
                    dct[index][internalKey] = subDict[internalKey]
            else:
                dct[index] = subDict
        else:
            dct[index] = subDict
    return dct


def dictify(lst: list or str, defaultValue: object = None):
    if not lst:
        return defaultValue

    dct = {}
    for key in (lst if type(lst) is list else [lst]):
        dct = addToDict(dct, key, defaultValue)
    return dct
