import os


def getPath(path: str, key: str = "dir", **kwargs):
    fullPath = "%s%s%s" % (
        kwargs.get(key, os.path.curdir), os.path.sep, path)
    if kwargs.get("ensure", False):
        content = fullPath.split(os.path.sep)
        os.makedirs(fullPath.replace(
            content[len(content) - 1], ""), exist_ok=True)
    return fullPath
