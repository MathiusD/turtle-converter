class BaseClass:

    @property
    def constructorKwargs(self):
        return {"class": self.__class__}
