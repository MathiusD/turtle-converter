from ..utils import getRequireData
from .filters import isValid
from .located import LocatedField


class RetainedField(LocatedField):

    def __init__(self, retainedFieldData: dict):
        self.value = getRequireData(
            "value", retainedFieldData, **self.constructorKwargs)
        self.filter = getRequireData(
            "filter", retainedFieldData, **self.constructorKwargs)
        super().__init__(retainedFieldData)

    def isRetained(self, entity: dict, **kwargs):
        data = self.getFromEntity(entity, **kwargs)
        if data:
            return isValid(self.filter, self.value, data)
        return False
