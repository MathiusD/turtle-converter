from .base_requirements import BaseRequirementsInstruction

from ..filters import isValid


class ConditionalInstruction(BaseRequirementsInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'conditional',
            'ternary'
        ]

    @staticmethod
    def requirements():
        return [
            'filter',
            'target'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        if cls.respectRequirements(**kwargs):
            returnValue = kwargs.get('return', {})
            match = kwargs.get('match', True)
            if isValid(kwargs.get('filter'), kwargs.get('target'), current) is match:
                return returnValue.get('valid', current)
            else:
                return returnValue.get('invalid', None)
        else:
            return None
