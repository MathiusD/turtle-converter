from .base import BaseInstruction


class GetInstruction(BaseInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'get',
            'base'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        return current if current else last
