from .base_requirements import BaseRequirementsInstruction


class ConstInstruction(BaseRequirementsInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'const'
        ]

    @staticmethod
    def requirements():
        return [
            'value'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        if cls.respectRequirements(**kwargs):
            return kwargs.get('value')
        else:
            return None
