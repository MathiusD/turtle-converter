import copy

from ...utils import makeKeyDict
from .case import CaseInstruction
from .conditional import ConditionalInstruction
from .const import ConstInstruction
from .fetch import FetchInstruction
from .get import GetInstruction
from .math import _mathInstructions
from .sparql import SparqlInstruction
from .split import SplitInstruction
from .utils import getFromEntity as _getFromEntity
from .utils import process as _process

_instructions = copy.deepcopy(_mathInstructions)
_instructions.append(CaseInstruction)
_instructions.append(ConditionalInstruction)
_instructions.append(ConstInstruction)
_instructions.append(FetchInstruction)
_instructions.append(GetInstruction)
_instructions.append(SparqlInstruction)
_instructions.append(SplitInstruction)

instructions = makeKeyDict(_instructions)

base = GetInstruction


def process(instructionName: str, last, current, **kwargs):
    return _process(instructionName, last, current, instructions, base, **kwargs)


def getFromEntity(entity: dict, location: str, **kwargs):
    return _getFromEntity(entity, location, instructions, base, **kwargs)


__all__ = [
    "base",
    "getFromEntity"
    "instructions",
    "process",
]
