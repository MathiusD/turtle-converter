import abc

from ...utils import ClassWithKeyAssociated


class BaseInstruction(ClassWithKeyAssociated):

    @classmethod
    @abc.abstractmethod
    def process(cls, last, current, **kwargs):
        pass
