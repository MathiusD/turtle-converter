from .base_requirements import BaseRequirementsInstruction


class SplitInstruction(BaseRequirementsInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'split'
        ]

    @staticmethod
    def requirements():
        return [
            'separator'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        if current and cls.respectRequirements(**kwargs) and type(current) == str:
            split = current.split(kwargs.get('separator'))
            if 'index' in kwargs.keys():
                index = kwargs.get('index')
                if type(index) == int and index < len(split):
                    return split[index]
            return split
        else:
            return None
