import abc

from .base import BaseInstruction


class BaseRequirementsInstruction(BaseInstruction):

    @staticmethod
    @abc.abstractmethod
    def requirements():
       pass

    @classmethod
    def respectRequirements(cls, **kwargs):
        for requirement in cls.requirements():
            if not requirement in kwargs.keys():
                return False
        return True
