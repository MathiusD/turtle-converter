from ....utils import makeKeyDict
from .divide import DivideInstruction
from .max import MaxInstruction

_mathInstructions = [
    DivideInstruction,
    MaxInstruction
]

mathInstructions = makeKeyDict(_mathInstructions)

__all__ = [
    "mathInstructions"
]
