from ..base import BaseInstruction


class MaxInstruction(BaseInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'max'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        if current:
            maxValue = kwargs.get("max", None)
            return current if maxValue and current <= maxValue else maxValue
        else:
            return current
