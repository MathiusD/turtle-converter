from ..base import BaseInstruction


class DivideInstruction(BaseInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'divide'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        return last / current if current and current != 0 and last else None
