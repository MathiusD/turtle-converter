from .base_requirements import BaseRequirementsInstruction

from ..filters import isValid


class CaseInstruction(BaseRequirementsInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'case'
        ]

    @staticmethod
    def requirements():
        return [
            'filter',
            'values'
        ]

    @classmethod
    def process(cls, last, current, **kwargs):
        if cls.respectRequirements(**kwargs):
            for key, val in kwargs.get('values').items():
                if isValid(kwargs.get('filter'), key, current):
                    return val
            return None
        else:
            return None
