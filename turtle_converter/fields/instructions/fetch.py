import os
import tempfile

from ...fetcher import fetch, protocolOfUri
from ...loader import load
from ...utils import getPath
from .base_requirements import BaseRequirementsInstruction
from .utils import getValueFromEntity


class FetchInstruction(BaseRequirementsInstruction):

    @classmethod
    def keyAssociated(cls):
        return [
            'fetch'
        ]

    @staticmethod
    def requirements():
        return [
            'uri',
            'format'
        ]

    @staticmethod
    def defaultLocation():
        return "%s%s%s" % (
            tempfile.gettempdir(), os.path.sep,
            os.path.basename(os.path.abspath(os.path.curdir))
        )

    @classmethod
    def process(cls, last, current, **kwargs):
        if current and cls.respectRequirements(**kwargs):
            uri = kwargs.get('uri').replace(
                kwargs.get('keyword', '$current'), current)
            formatSpec = kwargs.get('format')
            protocol = protocolOfUri(uri)
            dirPath = '%s%s%s' % (kwargs.get(
                "cacheDir", cls.defaultLocation()), os.sep, protocol)
            basePath = "%s.%s" % (uri.replace(
                "%s://" % protocol, ""), formatSpec)
            path = basePath
            lastSection = None
            for section in basePath.split("/"):
                if lastSection:
                    dirPath = "%s%s%s" % (dirPath, os.sep, lastSection)
                path = section
                lastSection = section
            dataPath = {'inDir': dirPath}
            fullPath = getPath(path, "inDir", **dataPath)
            targetExist = os.path.exists(fullPath)
            if not targetExist:
                targetExist = fetch(uri, path, **dataPath)
            if not targetExist:
                return None
            data = load(fullPath, formatSpec)
            if 'only' in kwargs.keys():
                return getValueFromEntity(data, kwargs.get('only'))
            else:
                return data
        else:
            return None
