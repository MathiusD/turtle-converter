from ...utils import makeKeyDict
from .equal import EqualFilter
from .start_with import StartWithFilter

_filters = [
    EqualFilter,
    StartWithFilter
]

filters = makeKeyDict(_filters)


def isValid(filterName: str, target, value, **kwargs):
    if filterName in filters.keys():
        return filters[filterName][0].isValid(target, value, **kwargs)
    else:
        raise Exception(
            "Not filter register for %s" % filterName)


__all__ = [
    "filters",
    "isValid"
]
