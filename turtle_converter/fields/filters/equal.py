from .base import BaseFilter


class EqualFilter(BaseFilter):

    @classmethod
    def keyAssociated(cls):
        return [
            'equals'
        ]

    @classmethod
    def isValid(cls, target, data, **kwargs):
        return target == data
