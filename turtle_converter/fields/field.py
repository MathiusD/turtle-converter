from .base import BaseField
from .located import LocatedField


class Field(BaseField, LocatedField):

    def __init__(self, fieldData: dict):
        BaseField.__init__(self, fieldData)
        LocatedField.__init__(self, fieldData)
