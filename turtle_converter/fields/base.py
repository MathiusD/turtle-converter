from ..utils import BaseClass, getRequireData


class BaseField(BaseClass):

    def __init__(self, baseFieldData: dict):
        self.relation = getRequireData(
            "relation", baseFieldData,  ** self.constructorKwargs)
        self.type = baseFieldData.get('type', None)
        self.isLink = baseFieldData.get('isLink', None)
        self.isRDF = baseFieldData.get('isRDF', None)
