from .utils import BaseClass, getRequireData


class Prefix(BaseClass):

    def __init__(self, prefixData: dict):
        self.name = getRequireData(
            "name", prefixData, **self.constructorKwargs)
        self.uri = getRequireData(
            "uri", prefixData, **self.constructorKwargs)
