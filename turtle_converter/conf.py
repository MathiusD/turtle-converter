import logging
import os

from .blueprint import Blueprint
from .fuseki import FusekiServer
from .loader import load
from .prefix import Prefix
from .target import Target
from .utils import BaseClass, getLogLevel, getPath, getRequireData, log


class ConfLoader(BaseClass):

    def __init__(self, confData: dict, autoConfirm: bool = None, loadServer: bool = True, verbositySpecified: bool = False):
        self.prefix = []
        for prefix in confData.get("prefix", []):
            self.prefix.append(Prefix(prefix))
        self.blueprints = {}
        for blueprintData in confData.get("blueprints", []):
            for key in blueprintData.keys():
                self.blueprints[key] = Blueprint(
                    blueprintData[key], self.prefix, self.blueprints)
        self.targets = []
        for target in getRequireData("targets", confData, **self.constructorKwargs):
            self.targets.append(Target(target, self.prefix, self.blueprints))
        self.indent = confData.get("indent", None)
        self.inDir = confData.get("inDir", os.path.curdir)
        self.outDir = confData.get("outDir", os.path.curdir)
        self.cacheDir = confData.get("cacheDir", None)
        if "verbose" in confData.keys() and not verbositySpecified:
            verbosity = getLogLevel(confData.get("verbose"))
            logging.root.setLevel(verbosity)
        self.serverRelated = FusekiServer(confData.get(
            "server"), loadServer) if "server" in confData.keys() else None
        self.autoConfirm = autoConfirm if autoConfirm is not None else confData.get(
            "autoConfirm", False)

    @property
    def kwargsRelated(self):
        return {
            "indent": self.indent,
            "inDir": self.inDir,
            "outDir": self.outDir,
            "autoConfirm": self.autoConfirm,
            "cacheDir": self.cacheDir
        }

    @classmethod
    def loadConf(cls, path: str, autoConfirm: bool = None, loadServer: bool = True, verbositySpecified: bool = False):
        log(logging.INFO, "Load conf from %s" % path)
        return cls(load(path), autoConfirm, loadServer, verbositySpecified)

    def targetsOnly(self, only: list = None):
        if only is None:
            return self.targets
        else:
            onlyRefined = []
            for data in only:
                onlyRefined.append(data.upper())
            out = []
            for target in self.targets:
                if target.name.upper() in onlyRefined:
                    out.append(target)
            return out

    def loadTargets(self, only: list = None):
        targetLoaded = {}
        targetUsed = self.targetsOnly(only)
        length = len(targetUsed)
        for index, target in enumerate(targetUsed):
            log(logging.INFO, "Load %s (%s/%s)" %
                (target.name, index + 1, length))
            targetLoaded[target] = target.loadTarget(
                **self.kwargsRelated)
        return targetLoaded

    def writeTtl(self, only: list = None):
        targetWrited = {}
        targetUsed = self.targetsOnly(only)
        length = len(targetUsed)
        for index, target in enumerate(targetUsed):
            log(logging.INFO, "Write %s (%s/%s)" %
                (target.name, index + 1, length))
            targetWrited[target] = target.writeTtl(
                **self.kwargsRelated)
        return targetWrited

    def sendTtl(self, only: list = None):
        if not self.serverRelated:
            raise AttributeError("Server must be defined for send Ttl")
        targetSend = {}
        if self.serverRelated.clearBeforeUpdate:
            log(logging.INFO, "clearBeforeUpdate is activated, launch remove and creation of dataset before batch update")
            self.serverRelated.removeDataSet(**self.kwargsRelated)
            if not self.serverRelated.dataSetExist:
                self.serverRelated.createDataSet(**self.kwargsRelated)
        log(logging.INFO, "Start batch update of Ttl")
        targetUsed = self.targetsOnly(only)
        length = len(targetUsed)
        for index, target in enumerate(targetUsed):
            log(logging.INFO, "Send %s (%s/%s)" %
                (target.name, index + 1, length))
            try:
                targetSend[target] = self.serverRelated.sendTtl(
                    getPath(target.outPath, "outDir", **self.kwargsRelated),
                    **self.kwargsRelated)
            except AttributeError:
                targetSend[target] = False
        return targetSend
