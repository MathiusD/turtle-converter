from enum import Enum


class FusekiDataSetType(Enum):
    IN_MEMORY = "mem"
    PERSISTANT_TDB = "tdb"
    PERSISTANT_TDB2 = "tdb2"


fusekiDataSetTypeList = {
    FusekiDataSetType.IN_MEMORY.value.lower(): FusekiDataSetType.IN_MEMORY,
    FusekiDataSetType.PERSISTANT_TDB.value.lower(): FusekiDataSetType.PERSISTANT_TDB,
    FusekiDataSetType.PERSISTANT_TDB2.value.lower(): FusekiDataSetType.PERSISTANT_TDB2
}
