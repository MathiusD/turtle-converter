from ..utils import BaseClass, getRequireData


class FusekiService(BaseClass):

    def __init__(self, fusekiService: dict):
        self.type = getRequireData(
            "srv.type", fusekiService, **self.constructorKwargs)
        self.description = getRequireData(
            "srv.description", fusekiService, **self.constructorKwargs)
        self.endpoints = []
        for endpoint in getRequireData(
                "srv.endpoints", fusekiService, **self.constructorKwargs):
            if len(endpoint) > 0:
                self.endpoints.append(endpoint)
        if len(self.endpoints) == 0:
            raise AttributeError("At least one endpoint must be specified")
