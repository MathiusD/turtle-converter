import logging

from ..utils import BaseClass, getRequireData, log
from .endpoints import fusekiEndpointList
from .service import FusekiService


class FusekiDataSet(BaseClass):

    def __init__(self, fusekiDataSetData: dict):
        self.name = getRequireData(
            "ds.name", fusekiDataSetData, **self.constructorKwargs)
        self.state = getRequireData(
            "ds.state", fusekiDataSetData, **self.constructorKwargs)
        self.services = {}
        for service in getRequireData(
                "ds.services", fusekiDataSetData, **self.constructorKwargs):
            try:
                data = FusekiService(service)
                if data.type.lower() in fusekiEndpointList.keys():
                    self.services[fusekiEndpointList[data.type.lower()]] = data
            except AttributeError as error:
                log(logging.DEBUG, "Error are catch in service fetch : %s" % error)
