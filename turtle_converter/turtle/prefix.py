def generatePrefix(name: str, uri: str):
    return "@prefix %s: <%s> ." % (name, uri)
