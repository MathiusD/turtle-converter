import abc
import logging

from .fetcher import fetch
from .utils import log


class Fetchable:

    def __init__(self, uri: str, path: str, typeProvided: str, requirements: list = None):
        self.uri = uri
        self.path = path
        self.typeProvided = typeProvided
        self.requirements = requirements if requirements else []

    def fetch(self, **kwargs):
        for requirement in self.requirements:
            if not requirement.usable(**kwargs):
                log(logging.DEBUG, "Requirement at %s isn't usable" % requirement.path)
                return False
        return fetch(self.url, self.path, **kwargs)

    @abc.abstractmethod
    def usable(self, **kwargs):
        pass
