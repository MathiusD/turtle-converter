== Turtle Converter

image:https://gitlab.com/MathiusD/turtle-converter/badges/master/pipeline.svg[link="https://gitlab.com/MathiusD/turtle-converter/-/pipelines",title="Pipeline Status"]
image:https://gitlab.com/MathiusD/turtle-converter/badges/master/coverage.svg[link="https://mathiusd.gitlab.io/turtle-converter/reports/coverage",title="Coverage Report"]
https://mathiusd.gitlab.io/turtle-converter/reports/mutation[Mutation Report]
https://mathiusd.gitlab.io/turtle-converter/docs/en[Sphinx Docs - En]
https://mathiusd.gitlab.io/turtle-converter/docs/fr[Sphinx Docs - Fr]

Un simple convertisseur de données en fichier Turtle. Ceci part à la base d'un projet de cours réalisé lors de cursus en M1 ALMA. Ce projet nous demandais de travailler sur un dataset public (Ici le déployement de la fibre en France). Cependant au cours du projet au vu du nombre hétéroclite de données et de format de données à traiter nous avons bricolé ce convertisseur.

=== Usage

Ce module python a pour but d'être utilisé via cli. Voici quelques exemples :


```bash
# Build from conf.yml in current dir
python3 -m turtle_converter build
# Validate conf at conf.example.yml
python3 -m turtle_converter validate -c conf.example.yml
# Load target of conf from conf.yml in current dir
python3 -m turtle_converter load
# Build and upload only target named "foo" from conf.yml in current dir
python3 -m turtle_converter build upload -o foo
```

=== Configuration

Vous pouvez trouver 2 exemples de configurations au sein du projet.

* `conf.default.yml` qui represente la configuration que nous utilisons pour notre projet évoqué plus haut
* `conf.default.json` qui represente une configuration minimale basé sur sur le fichier de données relatifs aux circuits de la TAN

=== Dataset relatif à la fibre (Projet de M1 ALMA)

Pour les personnes qui souhaitent utiliser notre dataset, voici quelques informations :

* Notre dataset couvre les données allant du second trimestre de 2015 jusqu'au second trimestre de 2015 (Nous l'étoffons au fur eet à mesure que nous le pouvons [Ou que nous avons la motivation])

* Notre dataset est construit à partir des données relativement au déployement du Haut et Très Haut Débit en France située ici :

** https://public.opendatasoft.com/explore/dataset/niveau-des-debits-sur-les-reseaux-dacces-a-internet-adsl-cable-fibre-ftth/information/

** https://www.data.gouv.fr/fr/datasets/niveau-des-debits-sur-les-reseaux-dacces-a-internet-adsl-cable-fibre-ftth-t2-2015-t2-2017/

** https://www.data.gouv.fr/fr/datasets/le-marche-du-haut-et-tres-haut-debit-fixe-deploiements/

* Si vous souhaitez utiliser nos données vous pouvez le faire via 3 méthodes :

** Utiliser notre endpoint sparql public : https://fuseki.alma.mathius.fr/fibre/sparql (Cependant ce dernier ne contient ici que les données du 44, histoire de ne pas trop demander à notre serveur [Effectivement, ici c'est un serveur personnel que nous utilisons nous ne souhaitions pas consommer nos crédits étudiants sur des solutions cloud tel que GCP, Azure ou AWS])

** Utiliser les dernières générations de nos TTL que nous mettons à jour aussi souvent que nécessaire ici : https://uncloud.univ-nantes.fr/index.php/s/MfGwG99YgEWRbHd

** Reconstruire les données depuis les fichier de conf conf.default.yml via la commande suivante après avoir cloné le dépôt courant : `python3 -m turtle_converter build -c conf.default.yml` (Et si vous souhaitez l'upload sur un fuseki via notre cli, pensez à mettre à jour les données au sein de l'attribut server au sein du fichier de configuration et la commande sera alors la suivante : `python3 -m turtle_converter build upload -c conf.default.yml`)
